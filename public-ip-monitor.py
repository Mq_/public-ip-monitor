#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import yaml
import socket
import os
import time
import sys
import syslog
import argparse
import csv
from datetime import datetime


def main():

    config = {}

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-p", "--print", action="store_true", help="Print database instead of performing IP check.")

    args = parser.parse_args()

    # change working directory to location of script
    os.chdir(os.path.dirname(__file__))

    # load default config
    if os.path.exists("config/default.yml"):
        with open("config/default.yml") as f:
            config = yaml.load(f, Loader=yaml.Loader)
    else:
        print("Could not find \"config/default.yml\".")
        syslog.syslog("Could not find \"config/default.yml\".")
        sys.exit(1)

    # load local config if it exists
    if os.path.exists("config/local.yml"):
        with open("config/local.yml") as f:
            config.update(yaml.load(f, Loader=yaml.Loader))


    if args.print:
        prettyprint(config)
        sys.exit(0)


    syslog.syslog("Running IP check.")

    # save timestamp of this run
    with open(config["last_run_file"], "w") as f:
        f.write(str(int(time.time())))

    ip = None

    # find ip of the supervised domain
    try:
        ip = socket.gethostbyname(config["domain"])
    except Exception as e:
        syslog.syslog("Exception: {}".format(e))
    except:
        syslog.syslog("Uncaught Error.")

    if ip is None:
        syslog.syslog("Could not get IP address.")
        sys.exit(1)

    # load last ip if it exists
    last_ip = ""
    if os.path.exists(config["last_ip_file"]):
        with open(config["last_ip_file"]) as f:
            last_ip = f.read()

    # write new ip to database if it has changed
    if last_ip != ip:
        syslog.syslog("Found new IP: {}".format(ip))
        # save last ip for comparison in next check
        with open(config["last_ip_file"], "w") as f:
            f.write(ip)
        # write database
        with open(config["database"], "a") as f:
            f.write("{},{},{}\n".format(int(time.time()), config["domain"], ip))


def prettyprint(config):
    if not config:
        syslog.log("Not config provided.")
        sys.exit(1)
    if "database" not in config:
        syslog.log("Could not find database in config.")
        sys.exit(1)
    if not os.path.exists(config["database"]):
        syslog.log("Database does not exist.")
        sys.exit(1)

    with open(config["database"], "r") as f:
        reader = csv.reader(f)
        for row in reader:
            print("{}\t{}\t{}".format(datetime.fromtimestamp(int(row[0])), row[1], row[2]))

if __name__ == '__main__':
    main()
